======= RELEASES =======
## v0.92
- emacs powerpack now only sets up M-x shell to use msys2 if msys2 can actually be found
- fix for swapped cases in shell setup logic

## v0.91
- includes emacs 25.3
- a fix for a screen that incorrectly stated the emacs version
- some code cleanup
- some fixed project metadata

## v0.9
- includes emacs 25.1
- drops integrated msys installer (instead I recommend that you install msys2 using the installer on their website)

## v0.2
includes emacs 24.5

======= SOURCE =======
Development for the project occurs here: https://gitlab.com/vancan1ty/emacs_powerpack

Please report any bugs here: https://gitlab.com/vancan1ty/emacs_powerpack/issues
